const express = require('express')
const cors = require('cors')
const cookie = require('cookie-parse')
const comp = require('compression')

require('../../database')

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(cors())
app.use(comp())
app.use(cookie())

module.exports = app
