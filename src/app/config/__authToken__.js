require('dotenv').config()
const jwtHandler = require('jsonwebtoken')

module.exports = (request, response, next) => {
  const authHeader = request.headers.authorization
  if (!authHeader) response.status(400).json({ warning: 'Token not Provider' })

  const [, token] = authHeader.split(' ')

  try {
    const payload = jwtHandler.verify(process.env.APP_SECRET, token)
    request.userId = payload.id
    next()
  } catch (error) {
    response.status(400).json({ warning: error })
  }
}
