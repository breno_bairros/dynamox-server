const Sequelize = require('sequelize')
const dbConfig = require('../app/config/database')

const User = require('../app/models/User')
const Category = require('../app/models/Category')
const Product = require('../app/models/Product')

const connection = new Sequelize(dbConfig)

User.init(connection)
Category.init(connection)
Product.init(connection)

Category.associate(connection.models)
Product.associate(connection.models)

module.exports = connection
