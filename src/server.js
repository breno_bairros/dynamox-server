const server = require('./app/config/express')
const normalizePort = require('./app/config/normalize')
server.listen(normalizePort(process.env.PORT || '3333'))
console.info('Servidor rodando!')
