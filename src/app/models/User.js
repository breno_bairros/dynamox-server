const { Model, DataTypes } = require('sequelize')
const bcrypt = require('bcryptjs')

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        hash: DataTypes.VIRTUAL,
      },
      {
        sequelize,
      },
    )

    this.addHook('beforeSave', async user => {
      if (user.hash) user.password = await bcrypt.hash(this.hash, 8)
    })
    return this
  }

  checkPassword(hash) {
    return bcrypt.compare(hash, this.password)
  }
}
module.exports = User
