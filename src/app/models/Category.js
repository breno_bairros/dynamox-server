const { Model, DataTypes } = require('sequelize')

class Category extends Model {
  static init(sequelize) {
    super.init(
      {
        category: DataTypes.STRING,
      },
      { sequelize },
    )
  }

  static associate(models) {
    this.belongsToMany()
  }
}

module.exports = Category
