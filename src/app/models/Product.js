const { Model, DataTypes } = require('sequelize')

class Product extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        manufacturingDate: DataTypes.STRING,
        perishableProduct: DataTypes.BOOLEAN,
        expirationDate: DataTypes.STRING,
        price: DataTypes.INTEGER,
      },
      { sequelize },
    )
  }

  static associate(models) {
    this.belongsTo(models.Category, {
      foreignKey: 'category_id',
      as: 'Category',
    })
  }
}

module.exports = Product
