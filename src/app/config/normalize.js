module.exports = args => {
  let port = parseInt(args, 10)

  if (isNaN(port)) console.info(args)
  if (port >= 0) return port

  return false
}
